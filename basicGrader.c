#include <stdio.h>

void printFinalGrades(float testGrade, float attendanceGrade, float classworkGrade, float homeworkGrade);
float averageTestGrade(float tGrade1,float tGrade2,float tGrade3,float tGrade4);
char letterGrade(float grade);
int main ()
{
	/*
	 *getting the input for the 4 test grades 
	 */
	float testGrade1, testGrade2, testGrade3, testGrade4, attendanceGrade, classworkGrade, homeworkGrade;
	printf("Please enter the four test grades\n");
	scanf("%f %f %f %f", &testGrade1, &testGrade2, &testGrade3, &testGrade4);
	float testGrade = averageTestGrade(testGrade1, testGrade2, testGrade3, testGrade4);
	/*
	 * getting the input for the remaining parts of the grade
	 */ 
	printf("Enter attendance grade\n");
	scanf("%f", &attendanceGrade);
	printf("Enter classwork grade\n");
	scanf("%f", &classworkGrade);
	printf("Enter homework grade\n");
	scanf("%f", &homeworkGrade);
	
	/*
	 * send everything to the methods that calculate the grade
	 */ 
	printFinalGrades(testGrade, attendanceGrade, classworkGrade, homeworkGrade);
	return 0;
}
void printFinalGrades(float tGrade, float attendanceGrade, float classworkGrade, float homeworkGrade)
{
	/*
	 * Calculating final grade
	 */
	float finalGrade = tGrade*.5 + attendanceGrade * .1 + classworkGrade * .15 + homeworkGrade * .25;
	char alphaGrade = letterGrade(finalGrade);
	/*
	 * printing out final grades
	 */
	printf("The final test grade is %.2f.\nThe attendance grade is %.2f.\n",tGrade, attendanceGrade);
	printf("The classwork grade is %.2f.\nThe homeowrk grade is %.2f.\n",classworkGrade,homeworkGrade);
	printf("The final grade is %.2f\nStudents grade final grade in the class is: %c\n", finalGrade, alphaGrade); 
}
float averageTestGrade(float tGrade1,float tGrade2,float tGrade3,float tGrade4)
{
	/*
	 * If the final 2 test grades are both greater than either of the first test grade
	 * drop the lowest test grade
	 */
	if((tGrade3 > tGrade1 || tGrade3 > tGrade2) && (tGrade4 > tGrade1 || tGrade4 > tGrade2))
	{
		printf("Lowest test grade will be dropped!\n");
		float tGrade = tGrade1;
		if(tGrade2 > tGrade)
			tGrade = tGrade2;
		return (tGrade+ tGrade3 + tGrade4) / 3;
	}
	return (tGrade1 + tGrade2 + tGrade3 + tGrade4)/4; 
}
char letterGrade(float grade)
{
	/*
	 * Not accounting for numbers below 0 or greater than 100
	 * if greater than 100 then it's going to be an A anyway
	 * below 0 is just an error, should throw an error
	 */
	if(grade < 60)
		return 'F';
	if(grade < 70)
		return 'D';
	if(grade < 80)
		return 'C';
	if(grade < 90)
		return 'B';
	return 'A';
}
